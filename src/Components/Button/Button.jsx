import React, {Component} from 'react';
import './Button.scss'
class Button extends Component {

    render(){
        const {textBtn,onClick,background} = this.props;
return (
   
        <button className={background} type="button" onClick={onClick}>{textBtn}</button>
    
)
    }

}
export default Button;